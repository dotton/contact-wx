// 初始化AV
var Bmob = require('utils/bmob.js');
Bmob.initialize("ec5423302bee8a4672494a6ee0e90c58", "c2069b45a64725b08445063b3d59bf1c");

// 授权登录
App({
  onLaunch: function() {
        this.login();
    },
    login: function() {
        var user = new Bmob.User;
        if (Bmob.User.current()) {
        	// console.log(Bmob.User.current());
            return;
        }
        wx.login({
            success: function(res) {
                user.loginWithWeapp(res.code)
            }
        });
    }
})
