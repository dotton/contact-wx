let Bmob = require('../../utils/bmob.js');
let validate = require('../../utils/validate.js');
let _ = require('../../utils/underscore.js');

Page({
	data: {
		contactObjects: [],
		visual: false,
		animate: '',
		fade: '',
		isEditing: false,
		contact: {},
	},
	onLoad: function () {
		this.loadData();
	},
	add: function () {
		this.setData({
			isEditing: false,
			contact: {},
		});
		this.show();
	},
	save: function (e) {
		// todo 数据有效性表单验证
		let data = e.detail.value;
		if (!validate.required({
				message: '联系人不能为空',
				value: data.title
			})) {
			return;
		}
		if (!validate.required({
				message: '号码不能为空',
				value: data.phone
			})) {
			return;
		}
		// 加载中
		wx.showLoading({
			title: '保存中'
		});
		// 区分对待添加与修改
		let contact = new Bmob.Object('Contact');
		// 引入当前用户
		data.user = Bmob.User.current();
		// 当编辑时，取data存储的contact
		if (this.data.isEditing) {
			contact = this.data.contact;
			_.extend(contact, data);
		}
		contact.save(data).then(res => {
			wx.hideLoading();
			// console.log(res);
			wx.showToast({
				title: '保存成功',
				duraction: .5,
				success: () => {
					this.hide();
					this.loadData();
				}
			});
		});
	},
	loadData: function () {
		var query = new Bmob.Query('Contact');
		query.equalTo('user', Bmob.User.current());
		query.ascending('title');
		query.limit(Number.MAX_VALUE);
		query.find().then(contactObjects => {
			// console.log(contactObjects)
			wx.stopPullDownRefresh();
			this.setData({
				contactObjects: contactObjects,
			});
		});
	},
	hide: function () {
		// 隐藏modal弹窗
		this.setData({
			// visual: false,
			animate: 'bounceOut',
			fade: 'fadeOut',
			isEditing: false,
		});
		// 动画结束后应该隐藏之，防止隔空被点击
		setTimeout( ()=> {
			this.setData({
				visual: false,
			});
		}, 750);
	},
	show: function () {
		// 显示modal弹窗
		this.setData({
			visual: true,
			animate: 'bounceIn',
			fade: 'fadeIn',
		});
	},
	preventDefault: function () {
		// 什么都不做，只为阻止父元素点击hide事件
	},
	call: function (e) {
		var index = e.currentTarget.dataset.index;
		var phone = this.data.contactObjects[index].get('phone');
		wx.makePhoneCall({
			phoneNumber: phone
		})
	},
	read: function (e) {
		var index = e.currentTarget.dataset.index;
		// 存储result对象以供显示
		this.setData({
			contact: this.data.contactObjects[index],
			isEditing: true,
		});
		this.show();
	},
	delete: function (e) {
		var index = e.currentTarget.dataset.index;
		wx.showModal({
			title: '提示',
			content: '确定要删除吗',
			success: (res) => {
				if (res.confirm) {
					this.data.contactObjects[index].destroy({
						success: (deleteObject) => {
							wx.showToast({
								title: '删除成功'
							});
							this.loadData();
						},
						error: (object, error) => {
							console.log('删除失败');
						}
					});
				} else if (res.cancel) {
					// console.log('用户点击取消')
				}
			}
		})
	},
	onShareAppMessage: function () {
		return {
			title: '让通讯录更清爽',
			desc: '不常用的电话号码就存这里吧',
			path: '/pages/index/index'
		}
	},
	onPullDownRefresh: function() {
        // 下拉刷新
        this.loadData();
    },
})