var required = function(obj) {
    if (obj.value == undefined || obj.value == '') {
        wx.showToast({
            title: obj.message,
            showCancel: false
        });
        return false;
    }
    return true;
}

module.exports = {
    required: required,
};